package com.mlh.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
/**
*文件压缩
*/
public class ZipTool {
    
    public static int maxlevel = 3;
    public static int level = 0;
    
    public static void main(String[] args) {
        int loop = 1024 * 1024;
        int filenum=10;
        byte[] data = new byte[1024];
        printData(data);
        File targetfile = new File("D:/target.zip");
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        try {
            fos = new FileOutputStream(targetfile);
            zos = new ZipOutputStream(fos);
            for(int j=0;j<filenum;j++){
                ZipEntry entry = new ZipEntry("myentry_"+j);
                entry.setSize(data.length * loop);
                zos.putNextEntry(entry);
                for(int i=0;i<loop;i++){
                    zos.write(data);
                }
                zos.closeEntry();
                zos.flush();
                System.out.println(entry.getName()+" start");
            }
            zos.close();
            while(level++ < maxlevel){
                System.out.println("level:"+level);
                targetfile = zipFile(targetfile);
            }
            //file.delete();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static File zipFile(File target) {
        System.out.println("hehe");
        int loop = 10;
        byte[] data = new byte[(int)target.length()];
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(target);
            fis.read(data);
            fos = new FileOutputStream(target);
            zos = new ZipOutputStream(fos);
            
            for(int i=0;i<loop;i++){
                System.out.println("hh"+i);
                ZipEntry entry = new ZipEntry("targetentry_"+i+".zip");
                entry.setSize(data.length);
                zos.putNextEntry(entry);
                zos.write(data);
                zos.closeEntry();
                zos.flush();
            }
            zos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                fis.close();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return target;
    }
    
    public static void printData(byte[] data){
        for(int i=0;i<data.length;i++){
            System.out.print(data[i]+"\t");
        }
        System.out.println();
    }
}
