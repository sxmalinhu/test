package com.mlh.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;
/**
 *文件搜索1
*/
public class FileSearch {

	static PrintWriter pw = null;
	private static String keyword = null;
	private static boolean searchContent = false;// 是否查看文件内容
	static {
		try {
			keyword = "";// 查询文件
			pw = new PrintWriter(new File("D:/searchlog/searchlog_"+(searchContent?keyword+"_":"")
					+ System.currentTimeMillis() + ".txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static String filename = "ppt";// 查询文件
	private static String dirpath = "C:/Users/admin/Desktop/Trunk/trunk/source/MessageServer";// 查询路径
	private static long count = 0;// 统计文件总数
	private static boolean lookHiddenPath = false;// 是否查看隐藏文件夹
	
	private static String BM = "GBK";

	public static void main(String[] args) throws UnknownHostException,
			FileNotFoundException {
		// dirpath = "D:/";// 查询路径
		// dirpath = "C:/Users/malh/Desktop/Trunk/trunk/source/MessageServer/SRC";
		// dirpath = "C:/Users/malh/Desktop/20180126/sydj-rcp/bin";
		// dirpath = "C:/Users/malh/Desktop/Trunk/trunk/source/MessageServer/src";
		// dirpath = "E:/Malh/workspace/日常/源码分析/bcprov-jdk15on-160/bcprov-jdk15on-160.src";
		listAllFiles();
		//serachPart();
		//searchAll();
		System.out.println(keyword+"|查询出文件总数：" + count);
		writeToFile(keyword+"|查询出文件总数：" + count);
		pw.flush();
		pw.close();
	}

	// 列出指定路径下所有文件名
	public static void listAllFiles() throws UnknownHostException,
			FileNotFoundException {
		File dir = new File(dirpath);
		listAllFiles(dir);
		System.out.println("文件总数：" + count);
	}

	// 指定路径扫描
	public static void serachPart() throws UnknownHostException {
		File dir = new File(dirpath);
		search(dir, filename);
	}

	// 全盘扫描
	public static void searchAll() throws UnknownHostException {
		File[] roots = File.listRoots();
		for (File root : roots) {
			System.out.println("开始扫描磁盘：" + root.getPath() + "\thidden:"
					+ root.isHidden());
			search(root, filename);
		}
	}

	// 列出所有文件名，无限递归
	public static void listAllFiles(File file) throws FileNotFoundException {
		if (file.exists() && file.isFile()) {
			count++;
			System.out.println(file.getAbsolutePath());
			writeToFile(file.getAbsolutePath());
		} else if (file.isDirectory()) {// 判断隐藏路径
			if (lookHiddenPath || (!lookHiddenPath && !file.isHidden())
					|| file.getParent() == null) {
				System.out.println("目录：" + file.getAbsolutePath());
				writeToFile("目录：" + file.getAbsolutePath());
				File[] files = file.listFiles();
				if (files != null) {
					for (File subfile : files) {
						listAllFiles(subfile);
					}
				}
			}
		}
	}

	// 文件名搜索，无限递归
	public static void search(File file, String filename) {
		if (file.exists() && file.isFile()) {
			String fname = file.getName();
			if (fname.toUpperCase().contains(filename.toUpperCase())) {

				if (searchContent) {
					searchContent(file, keyword);
				}else{
					count++;
					System.out.println(file.getAbsolutePath());
					try {
						writeToFile(file.getAbsolutePath());
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		} else if (file.isDirectory()) {// 判断隐藏路径
			if (lookHiddenPath
					|| (!lookHiddenPath && !file.isHidden() || file.getParent() == null)) {
				File[] files = file.listFiles();
				if (files != null) {
					for (File subfile : files) {
						search(subfile, filename);
					}
				}
			}
		}
	}

	private static void searchContent(File file, String keyword) {
		BufferedReader br = null;
		int rownum = 0;
		boolean flag = false;
		if (file.isFile()) {
			try {
				br = new BufferedReader(new InputStreamReader(
						new FileInputStream(file), BM));
				String temp = "";
				while ((temp = br.readLine()) != null) {
					rownum++;
					String nStr = new String(temp.toUpperCase().getBytes());
					if (temp.toUpperCase().contains(keyword.toUpperCase())) {
						System.out.println(rownum + ":" + temp);
						writeToFile(rownum + ":" + temp);
						flag=true;
					} else if (nStr.contains(keyword.toUpperCase())) {
						System.out.println(rownum + ":" + temp);
						writeToFile(rownum + ":" + temp);
						flag=true;
					}
					// System.out.println(rownum+":"+nStr);
				}
				if(flag){
					count++;
					System.out.println(file.getAbsolutePath());
					writeToFile(file.getAbsolutePath());
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

    /**
     * 把文本信息输出到文件中
     */
	private static void writeToFile(String text) throws FileNotFoundException {
        pw.println(text);
    }
}
